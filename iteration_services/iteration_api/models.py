########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


from flask_sqlalchemy import SQLAlchemy
from qops_tablet import db


class Base(db.Model):
    __abstract__ = True
    __bind_key__ = 'qops'

    id = db.Column(db.Integer, primary_key = True)
    created_on = db.Column(db.DateTime, default=db.func.now())
    updated_on = db.Column(db.DateTime, default=db.func.now(), onupdate=db.func.now())


class Iteration(Base):
    __tablename__ = 'iteration'

    identifier = db.Column(db.String)
    sequence = db.Column(db.Integer)
    name = db.Column(db.String)
    theme_id = db.Column(db.Integer)
    date_begin = db.Column(db.DateTime)
    date_end = db.Column(db.DateTime)

    def get_next_identifier():
        return 'IT00001'


class IterationTheme(Base):
    __tablename__ = 'iteration_theme'

    identifier = db.Column(db.String)
    name = db.Column(db.String)
    usage = db.Column(db.String)


class ReleaseIterations(Base):
    __tablename__ = 'release_iterations'

    release_id = db.Column(db.Integer)
    iteration_id = db.Column(db.Integer)
