########################################################################
# Copyright 2018 Jason Alan Smith
#
# This file is part of Quality Operations Center.
#
# Quality Operations Center is free software: you can redistribute it
# and/or modify it under the terms of the GNU General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# Quality Operations Center is distributed in the hope that it will be
# useful, but WITHOUT ANY WARRANTY; without even the implied warranty
# of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Quality Operations Center.
# If not, see <https://www.gnu.org/licenses/>.
########################################################################


# Import flask dependencies
from flask import Blueprint, request, render_template, \
    flash, g, session, redirect, url_for

# Import models
from .models import Iteration
from .models import IterationTheme

from .forms import IterationProfileForm

import logging

# Import the database object from the main app module
from qops_desktop import db

# Define the blueprint: 'auth', set its url prefix: app.url/auth
mod_iteration = Blueprint('iteration', __name__, url_prefix='/iteration')


@mod_iteration.context_processor
def store():
    store_dict = {'serviceName': 'Iteration',
                  'serviceDashboardUrl': url_for('iteration.dashboard'),
                  'serviceBrowseUrl': url_for('iteration.browse'),
                  'serviceNewUrl': url_for('iteration.new'),
                  }
    return store_dict

# Set the route and accepted methods


@mod_iteration.route('/', methods=['GET'])
def iteration():
    return render_template('iteration/iteration_dashboard.html')


@mod_iteration.route('/dashboard', methods=['GET'])
def dashboard():
    return render_template('iteration/iteration_dashboard.html')


@mod_iteration.route('/browse', methods=['GET'])
def browse():
    iterations = Iteration.query.with_entities(Iteration.id, Iteration.sequence, Iteration.name, Iteration.date_begin, Iteration.date_end, IterationTheme.name.label(
        'theme_id')).join(IterationTheme, Iteration.theme_id == IterationTheme.id).all()
    return render_template('iteration/iteration_browse.html', iterations=iterations)


@mod_iteration.route('/new', methods=['GET', 'POST'])
def new():
    iteration = Iteration()
    form = IterationProfileForm(request.form)
    form.theme_id.choices = [(t.id, t.name)
                             for t in IterationTheme.query.all()]
    if request.method == 'POST':
        form.populate_obj(iteration)
        iteration.identifier = Iteration.get_next_identifier()
        db.session.add(iteration)
        db.session.commit()
        return redirect(url_for('iteration.browse'))
    return render_template('iteration/iteration_new.html', iteration=iteration, form=form)


@mod_iteration.route('/profile', methods=['GET', 'POST'])
@mod_iteration.route('/profile/<int:iteration_id>/profile', methods=['GET', 'POST'])
def profile(iteration_id=None):
    iteration = Iteration.query.get(iteration_id)
    form = IterationProfileForm(obj=iteration)
    form.theme_id.choices = [(t.id, t.name)
                             for t in IterationTheme.query.all()]
    if request.method == 'POST':
        form = IterationProfileForm(request.form)
        form.populate_obj(iteration)
        db.session.add(iteration)
        db.session.commit()
        return redirect(url_for('iteration.browse'))
    return render_template('iteration/iteration_profile.html', iteration=iteration, form=form)


@mod_iteration.route('/view', methods=['GET', 'POST'])
@mod_iteration.route('/view/<int:iteration_id>/view', methods=[
    'GET', 'POST'])
def iteration_view(iteration_id=None):
    #iteration = Iteration.query.get(iteration_id)
    form = IterationProfileForm(obj=iteration)
    form.theme_id.choices = [(t.id, t.name)
                             for t in IterationTheme.query.all()]
    if request.method == 'POST':
        form = IterationProfileForm(request.form)
        form.populate_obj(iteration)
        db.session.add(iteration)
        db.session.commit()
        return redirect(url_for('iteration.browse'))
    return render_template('iteration/iteration_view.html',
                           iteration=iteration, form=form)


@mod_iteration.route('/profile/dashboard', methods=['GET'])
@mod_iteration.route('/profile/<int:iteration_id>/dashboard', methods=['GET'])
def iteration_dashboard(iteration_id=None):
    if iteration_id:
        iteration = Iteration.query.get(iteration_id)
    else:
        iteration = None
    return render_template('iteration/iteration_single_dashboard.html', iteration=iteration)


@mod_iteration.route('/profile/tasks', methods=['GET'])
@mod_iteration.route('/profile/<int:iteration_id>/tasks', methods=['GET'])
def iteration_tasks(iteration_id=None):
    if iteration_id:
        iteration = Iteration.query.get(iteration_id)
    else:
        iteration = None
    return render_template('iteration/iteration_single_tasks.html', iteration=iteration)


@mod_iteration.route('/profile/communication', methods=['GET', 'POST'])
@mod_iteration.route('/profile/<int:iteration_id>/communication', methods=['GET'])
def iteration_communication(iteration_id=None):
    if iteration_id:
        iteration = Iteration.query.get(iteration_id)
    else:
        iteration = None
    return render_template('iteration/iteration_single_communication.html', iteration=iteration)


@mod_iteration.route('/profile/documents', methods=['GET', 'POST'])
@mod_iteration.route('/profile/<int:iteration_id>/documents', methods=['GET'])
def iteration_documents(iteration_id=None):
    if iteration_id:
        iteration = Iteration.query.get(iteration_id)
    else:
        iteration = None
    return render_template('iteration/iteration_single_documents.html', iteration=iteration)


@mod_iteration.route('/profile/builds', methods=['GET', 'POST'])
@mod_iteration.route('/profile/<int:iteration_id>/builds', methods=['GET'])
def iteration_builds(iteration_id=None):
    if iteration_id:
        iteration = Iteration.query.get(iteration_id)
    else:
        iteration = None
    return render_template('iteration/iteration_single_builds.html', iteration=iteration)


@mod_iteration.route('/profile/customers', methods=['GET', 'POST'])
@mod_iteration.route('/profile/<int:iteration_id>/customers', methods=['GET'])
def iteration_customers(iteration_id=None):
    if iteration_id:
        iteration = Iteration.query.get(iteration_id)
    else:
        iteration = None
    return render_template('iteration/iteration_single_customers.html', iteration=iteration)


@mod_iteration.route('/profile/user_stories', methods=['GET', 'POST'])
@mod_iteration.route('/profile/<int:iteration_id>/user-stories', methods=['GET'])
def iteration_user_stories(iteration_id=None):
    if iteration_id:
        iteration = Iteration.query.get(iteration_id)
    else:
        iteration = None
    return render_template('iteration/iteration_single_user_stories.html', iteration=iteration)
